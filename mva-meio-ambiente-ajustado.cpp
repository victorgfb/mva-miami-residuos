// --------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
// --------------------------------------------------------------------
using namespace std;
// --------------------------------------------------------------------

int main( void )
{

	int    N    = 2717000, n;


	// 	Central transfer station 
	// West transfer station
	// Northeast transfer station 
	// new transfer station

	double S0 =  0.000004233532673, V0 = 0.1162790698, D0 = S0*V0,
	S1 = 0.000002490313337, V1 = 0.1976744186, D1 = S1*V1,
	S2 = 0.000002645957921, V2 = 0.1860465116, D2 = S2*V2,
	S3 = 0.00000098454248, V3 = 0.5, D3 = S3*V3,


	// North Dade Landfill
	// South Dade Landfill
	// Ashfill
	// new Landfill

	S4 =  0.000003307447401, V4 = 0.1233178006, D4 = S4*V4,
	S5 =  0.000001693413069, V5 = 0.2408550792, D5 = S5*V5,
	S6 =  0.000008268618502, V6 = 0.04932712022, D6 = S6*V6,
	S7 =  0.00000098637760, V7 = 0.41350000000, D7 = S7*V7,

	// Resource recovery facility
	// Waste services of florida miami

	S8 =  0.0000009407850, V8 = 0.1415454545, D8 = S8*V8,
	S9 =  0.0000042335327, V9 = 0.03145454545, D9 = S9*V9,

	Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9,
	R0, R1, R2, R3, R4, R5, R6, R7, R8, R9,
	U0, U1, U2, U3, U4, U5, U6, U7, U8, U9,

	R, X, Z = 0.0;

	stringstream ss;
	ss << setprecision(5);
	ss << "n;R0;R1;R2;R3;R4;R5;R6;R7;R8;R9;R;X;Q0;Q1;Q2;Q3;Q4;Q5;Q6;Q7;Q8;Q9;U0;U1;U2;U3;U4;U5;U6;U7;U8;U9" << endl;
	Q0 = Q1 = Q2 = Q3 = Q4 = Q5 = Q6 = Q7 = 0.0;
	for( n = 1; n <= N; n++ ){
		R0 = S0*(1.0 + Q0);
		R1 = S1*(1.0 + Q1);
		R2 = S2*(1.0 + Q2);
		R3 = S3*(1.0 + Q3);
		R4 = S4*(1.0 + Q4);
		R5 = S5*(1.0 + Q5);
		R6 = S6*(1.0 + Q6);
		R7 = S7*(1.0 + Q7);
		R8 = S8*(1.0 + Q8);
		R9 = S9*(1.0 + Q9);

		R  = R0*V0 + R1*V1 + R2*V2 + R3*V3 + R4*V4 + R5*V5 + R6*V6 + R7*V7 + R8*V8 + R9*V9;
		X  = n/(R+Z);

		Q0 = X*R0*V0;
		Q1 = X*R1*V1;
		Q2 = X*R2*V2;
		Q3 = X*R3*V3;
		Q4 = X*R4*V4;
		Q5 = X*R5*V5;
		Q6 = X*R6*V6;
		Q7 = X*R7*V7;
		Q8 = X*R8*V8;
		Q9 = X*R9*V9;

		U0 = Q0/(1+Q0);
		U1 = Q1/(1+Q1);
		U2 = Q2/(1+Q2);
		U3 = Q3/(1+Q3);
		U4 = Q4/(1+Q4);
		U5 = Q5/(1+Q5);
		U6 = Q6/(1+Q6);
		U7 = Q7/(1+Q7);
		U8 = Q8/(1+Q8);
		U9 = Q9/(1+Q9);

		if(n>=2000000)
		{
		   ss << n  << ";"
			<< R0 << ";" << R1 << ";" << R2 << ";" << R3 << ";"<< R4 << ";"<< R5 << ";"<< R6 << ";"<< R7 << ";" << R8 << ";" << R9 << ";"
			<< R  << ";" << X  << ";"
			<< Q0 << ";" << Q1 << ";" << Q2 << ";" << Q3 << ";"<< Q4 << ";"<< Q5 << ";"<< Q6 << ";"<< Q7 <<";" << Q8 << ";" << Q9 << ";"
			<< U0 << ";" << U1 << ";" << U2 << ";" << U3 << ";"<< U4 << ";"<< U5 << ";"<< U6 << ";"<< U7 << ";"  << U8 <<";"<< U9 << ";" << endl;
		}
	}

	
	string str = ss.str();
	//cout << str;
	ofstream fo;
	replace(str.begin(), str.end(),'.',',');
	fo.open("mva-plantinhas-cenario-existente-2012.csv");
	//fo.open("MVA-Plantinhas.csv");
	fo << str;
	fo.close();
}
